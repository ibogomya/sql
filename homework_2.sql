use[Blog];

go

create table users
(
	id int not null primary key identity (1, 1),
	name nvarchar (50) not null,
	email nvarchar (50) not null
)

go

create table posts
(
	id int not null primary key identity (1, 1),
	textposts nvarchar(max) null,
	usersId int NOT NULL FOREIGN KEY REFERENCES users(Id)
	
)
go

create table comments 
(
	id int not null primary key identity (1, 1),
	textcomment nvarchar(max) null,
	usersId int NOT NULL FOREIGN KEY REFERENCES users(Id),
	postid int not null FOREIGN KEY REFERENCES  posts(id)
)

go 


create table tags
(
	id int not null primary key identity (1, 1),
	nametag nvarchar(50) not null,
	postsid int not null foreign key references posts(id)

)

INSERT INTO users 
(email,name)
SELECT B.AuthorEmail,B.Author
FROM Blog as B
GROUP BY B.Author,B.AuthorEmail

INSERT INTO posts
(textposts,usersId)
SELECT B.Post, U.id
FROM Blog as B
INNER JOIN users AS U ON u.name = B.Author
GROUP BY B.Post, U.id

INSERT INTO comments
(textcomment,usersId,postid)
SELECT B.Comment,U.id,P.id 
FROM Blog AS B
INNER JOIN users AS U ON U.name = B.Author
INNER JOIN posts AS P ON P.textposts = B.Post
GROUP BY B.Comment, U.id, P.id

INSERT INTO tags
(nametag,postsid)
SELECT B.Tag,P.id
FROM Blog AS B
INNER JOIN posts AS P ON P.textposts = B.Post
GROUP BY B.Tag,P.id


SELECT U.name,U.email,P.textposts,T.nametag, c.textcomment
FROM  users AS U
INNER JOIN posts AS P ON U.id = P.usersId
INNER JOIN comments AS C ON U.id = C.usersId and C.postid = P.id
INNER JOIN tags AS T ON P.id = T.postsid

drop table users
drop table posts
drop table comments
drop table tags


