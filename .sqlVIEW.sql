IF OBJECT_ID (N'dbo.GetOrderTotal', N'FN') IS NOT NULL  
    DROP FUNCTION GetOrderTotal;  
GO  

CREATE FUNCTION dbo.GetOrderTotal(@OrderId int)  
RETURNS money   
AS   

BEGIN  
  DECLARE @result money;
  SET @result = ( SELECT SUM(CONVERT(money, (OD.UnitPrice * OD.Quantity) * (1 - OD.Discount) / 100) * 100)
  FROM [Order Details] OD
  WHERE OrderID = @OrderId
  GROUP BY OrderID)

  IF(@result IS NULL)
    SET @result = 0;
  RETURN @result;
END;  
GO  

--SELECT C.ContactName AS C_ContactName
--   , E.FirstName AS E_FirstName
--   , E.LastName AS E_LastName
--  , dbo.GetOrderTotal(O.OrderID) AS OrderTotal
--FROM Orders O
--INNER JOIN Employees E ON E.EmployeeID = O.EmployeeID
--INNER JOIN Customers C ON C.CustomerID = O.CustomerID


USE[NORTHWND];
GO
IF OBJECT_ID (N'dbo.EMPLOYEES_NEW', N'V') IS NOT NULL  
    DROP VIEW EMPLOYEES_NEW;  
GO  
CREATE VIEW EMPLOYEES_NEW
AS 
SELECT E.FirstName,E.LastName,E.HireDate,E.Country, SUM(dbo.GetOrderTotal(O.OrderID)) AS ORDERTOTAL
FROM Employees  E
INNER JOIN Orders O ON E.EmployeeID  = O.EmployeeID
GROUP BY E.FirstName,E.LastName, E.HireDate, E.Country
GO

SELECT*
FROM EMPLOYEES_NEW
ORDER BY EMPLOYEES_NEW.FirstName, EMPLOYEES_NEW.LastName
